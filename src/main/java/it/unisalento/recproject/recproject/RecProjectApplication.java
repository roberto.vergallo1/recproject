package it.unisalento.recproject.recproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(RecProjectApplication.class, args);
    }

}
