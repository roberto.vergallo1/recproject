package it.unisalento.recproject.recproject.restcontrollers;

import it.unisalento.recproject.recproject.di.IRenewableSource;
import it.unisalento.recproject.recproject.domain.User;
import it.unisalento.recproject.recproject.dto.LoginDTO;
import it.unisalento.recproject.recproject.dto.UserDTO;
import it.unisalento.recproject.recproject.dto.UsersListDTO;
import it.unisalento.recproject.recproject.dto.AuthenticationResponseDTO;
import it.unisalento.recproject.recproject.exceptions.UserNotFoundException;
import it.unisalento.recproject.recproject.repositories.UserRepository;
import it.unisalento.recproject.recproject.security.JwtUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static it.unisalento.recproject.recproject.configuration.SecurityConfig.passwordEncoder;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    IRenewableSource solarPanel;

    @Autowired
    IRenewableSource windTurbine;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtilities jwtUtilities;

    @RequestMapping(value="/{id}", method= RequestMethod.GET)
    public UserDTO get(@PathVariable String id) throws UserNotFoundException {

        solarPanel.initialize();
        windTurbine.initialize();

        Optional<User> user = userRepository.findById(id);

        if(user.isEmpty()) {
            throw new UserNotFoundException();
        }

        UserDTO userDTO = new UserDTO();

        userDTO.setId(user.get().getId());
        userDTO.setNome(user.get().getNome());
        userDTO.setCognome(user.get().getCognome());
        userDTO.setEmail(user.get().getEmail());

        return userDTO;
    }

    @RequestMapping(value="/", method=RequestMethod.GET)
    public UsersListDTO getAll() {

        UsersListDTO usersList = new UsersListDTO();
        ArrayList<UserDTO> list = new ArrayList<>();
        usersList.setList(list);

        List<User> users = userRepository.findAll();

        for(User user : users) {
            UserDTO userDTO = new UserDTO();

            userDTO.setId(user.getId());
            userDTO.setNome(user.getNome());
            userDTO.setCognome(user.getCognome());
            userDTO.setEmail(user.getEmail());
            userDTO.setPassword(user.getPassword());

            list.add(userDTO);
        }

        return usersList;

    }


    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public UsersListDTO search(@RequestParam(required = false) String nome, @RequestParam String cognome) {

        UsersListDTO usersListDTO = new UsersListDTO();
        ArrayList<UserDTO> users = new ArrayList<>();
        usersListDTO.setList(users);

        //List<User> list = userRepository.findByCognome(cognome);
        List<User> list = userRepository.findByNomeAndCognome(nome, cognome);

        for(User user: list) {
            UserDTO userDTO = new UserDTO();
            userDTO.setId(user.getId());
            userDTO.setNome(user.getNome());
            userDTO.setCognome(user.getCognome());
            userDTO.setEmail(user.getEmail());
            users.add(userDTO);
        }

        return usersListDTO;

    }


    @RequestMapping(value="/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody LoginDTO loginDTO) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginDTO.getEmail(),
                        loginDTO.getPassword()
                )
        );

        User user = userRepository.findByEmail(authentication.getName());

        if(user == null) {
            throw new UsernameNotFoundException(loginDTO.getEmail());
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);

        final String jwt = jwtUtilities.generateToken(user.getEmail());

        return ResponseEntity.ok(new AuthenticationResponseDTO(jwt));

    }

}
