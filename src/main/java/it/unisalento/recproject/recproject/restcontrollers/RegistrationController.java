package it.unisalento.recproject.recproject.restcontrollers;

import it.unisalento.recproject.recproject.di.IRenewableSource;
import it.unisalento.recproject.recproject.domain.User;
import it.unisalento.recproject.recproject.dto.UserDTO;
import it.unisalento.recproject.recproject.dto.UsersListDTO;
import it.unisalento.recproject.recproject.exceptions.UserNotFoundException;
import it.unisalento.recproject.recproject.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static it.unisalento.recproject.recproject.configuration.SecurityConfig.passwordEncoder;

@RestController
@RequestMapping("/api/registration")
public class RegistrationController {

    @Autowired
    UserRepository userRepository;


    @RequestMapping(method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO post(@RequestBody UserDTO userDTO) {

        User user = new User();
        user.setNome(userDTO.getNome());
        user.setCognome(userDTO.getCognome());
        user.setEmail(userDTO.getEmail());
        user.setPassword(passwordEncoder().encode(userDTO.getPassword()));

        user = userRepository.save(user);
        userDTO.setId(user.getId());
        userDTO.setPassword(null);

        return userDTO;
    }



}
