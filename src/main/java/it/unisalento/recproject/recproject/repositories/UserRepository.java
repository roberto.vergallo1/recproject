package it.unisalento.recproject.recproject.repositories;

import it.unisalento.recproject.recproject.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserRepository extends MongoRepository<User, String> {

    List<User> findByCognome(String cognome);

    List<User> findByNomeAndCognome(String nome, String cognome);

    User findByEmail(String email);
}
