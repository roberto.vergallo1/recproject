package it.unisalento.recproject.recproject.di;

public interface IRenewableSource {

    void initialize();
}
